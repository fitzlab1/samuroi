import PIL
import numpy
import ScanImageTiffReader

def load_tif(filename):
    # shitty workaround for compatability with ScanImage Tiff files
    try:
        img = PIL.Image.open(filename)
        X,Y = img.size
        T = img.n_frames

        # workaround to get the dtype
        foo = numpy.array(img)
        data = numpy.ndarray(shape=(Y, X, T), dtype=foo.dtype)
        for i in range(T):
            img.seek(i)
            data[:, :, i] = numpy.array(img)

    except ValueError:
        with ScanImageTiffReader.ScanImageTiffReader(filename) as reader:
            img = reader.data()      # Note: dimensions are in [z, y, x] format
            reader.close()

        T, X, Y = img.shape
        foo = numpy.array(img)
        data = numpy.reshape(foo.T, (Y, X, T))

    return data
